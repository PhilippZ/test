#pragma once
#include <stdio.h>
#include "AnzeigenForm.h";
#include "EingebenForm.h";
#include "SuchenForm.h";
#include "AendernForm.h";
#include "LoeschenForm.h";

namespace INF3_Projekt {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Zusammenfassung f�r HauptForm
	/// </summary>
	public ref class HauptForm : public System::Windows::Forms::Form
	{
	public:
		HauptForm(void)
		{
			InitializeComponent();
			//
			//TODO: Konstruktorcode hier hinzuf�gen.
			//
		}

	protected:
		/// <summary>
		/// Verwendete Ressourcen bereinigen.
		/// </summary>
		~HauptForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::Button^  button5;
	private: System::Windows::Forms::Button^  button6;
	private: System::Windows::Forms::Button^  button7;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Label^  label2;
	protected:

	private:
		/// <summary>
		/// Erforderliche Designervariable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Erforderliche Methode f�r die Designerunterst�tzung.
		/// Der Inhalt der Methode darf nicht mit dem Code-Editor ge�ndert werden.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->button5 = (gcnew System::Windows::Forms::Button());
			this->button6 = (gcnew System::Windows::Forms::Button());
			this->button7 = (gcnew System::Windows::Forms::Button());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(103, 84);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(81, 45);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Datens�tze Anzeigen";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &HauptForm::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(103, 135);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(81, 45);
			this->button2->TabIndex = 1;
			this->button2->Text = L"Datensatz suchen";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &HauptForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(16, 84);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(81, 45);
			this->button3->TabIndex = 2;
			this->button3->Text = L"Datensatz eingeben";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->UseWaitCursor = true;
			this->button3->Click += gcnew System::EventHandler(this, &HauptForm::button3_Click);
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(16, 135);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(81, 45);
			this->button4->TabIndex = 3;
			this->button4->Text = L"Datensatz �ndern";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &HauptForm::button4_Click);
			// 
			// button5
			// 
			this->button5->Location = System::Drawing::Point(16, 186);
			this->button5->Name = L"button5";
			this->button5->Size = System::Drawing::Size(81, 45);
			this->button5->TabIndex = 4;
			this->button5->Text = L"Datensatz l�schen";
			this->button5->UseVisualStyleBackColor = true;
			this->button5->Click += gcnew System::EventHandler(this, &HauptForm::button5_Click);
			// 
			// button6
			// 
			this->button6->Location = System::Drawing::Point(415, 323);
			this->button6->Name = L"button6";
			this->button6->Size = System::Drawing::Size(109, 29);
			this->button6->TabIndex = 5;
			this->button6->Text = L"Daten speichern";
			this->button6->UseVisualStyleBackColor = true;
			// 
			// button7
			// 
			this->button7->Location = System::Drawing::Point(530, 323);
			this->button7->Name = L"button7";
			this->button7->Size = System::Drawing::Size(109, 29);
			this->button7->TabIndex = 6;
			this->button7->Text = L"Beenden";
			this->button7->UseVisualStyleBackColor = true;
			this->button7->Click += gcnew System::EventHandler(this, &HauptForm::button7_Click);
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 12, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label1->Location = System::Drawing::Point(12, 9);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(178, 20);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Studentendatenbank";
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label2->Location = System::Drawing::Point(13, 44);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(346, 16);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Willkommen! Bitte w�hlen Sie die gew�nschte Aktion aus.";
			// 
			// HauptForm
			// 
			this->AccessibleDescription = L"";
			this->AccessibleName = L"";
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(651, 364);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->label1);
			this->Controls->Add(this->button7);
			this->Controls->Add(this->button6);
			this->Controls->Add(this->button5);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->MaximumSize = System::Drawing::Size(667, 402);
			this->MinimumSize = System::Drawing::Size(667, 402);
			this->Name = L"HauptForm";
			this->Text = L"Studentendatenbank";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		

		AnzeigenForm^ANZ = gcnew AnzeigenForm();   // Objekt von Form2 erstellen:
		ANZ->ShowDialog();
	}
	private: System::Void button7_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	
		EingebenForm^EING = gcnew EingebenForm();   // Objekt von Form2 erstellen:
		EING->ShowDialog();
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {

		SuchenForm^SUCH = gcnew SuchenForm();		// Objekt von Form2 erstellen:
		SUCH->ShowDialog();
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
		AendernForm^AEND = gcnew AendernForm();		// Objekt von Form2 erstellen:
		AEND->ShowDialog();
}
private: System::Void button5_Click(System::Object^  sender, System::EventArgs^  e) {
	LoeschenForm^LOES = gcnew LoeschenForm();		// Objekt von Form2 erstellen:
	LOES->ShowDialog();
}
};
}
